package com.assetpanda.services;

import com.assetpanda.models.Contact;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ContactService {

    Contact get(String id);
    Contact save(Contact contact);
    Contact update(Contact contact);
    void delete(Contact contact);

    List<Contact> find(Contact contact);
}

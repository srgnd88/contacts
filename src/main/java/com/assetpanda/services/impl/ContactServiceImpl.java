package com.assetpanda.services.impl;

import com.assetpanda.models.Contact;
import com.assetpanda.services.ContactService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {

    private List<Contact> contactList;

    @PostConstruct
    public void init(){
        contactList = new ArrayList<>();

        Contact contact = new Contact();
        contact.setFirstName( "John");
        contact.setLastName( "Doe");
        contactList.add(contact);

        contact = new Contact();
        contact.setFirstName( "Jane");
        contact.setLastName( "Doe");
        contactList.add(contact);
    }

    @Override
    public Contact get(String id) {
        return null;
    }

    @Override
    public Contact save(Contact contact) {

        contactList.add( contact);
        return contact;
    }

    @Override
    public Contact update(Contact contact) {
        return null;
    }

    @Override
    public void delete(Contact contact) {

        contactList.remove(contact);
    }

    @Override
    public List<Contact> find(Contact contactQuery) {

        return contactList;
    }
}

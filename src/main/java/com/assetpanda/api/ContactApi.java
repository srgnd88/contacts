package com.assetpanda.api;

import com.assetpanda.models.Contact;
import com.assetpanda.services.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "/contact")
public class ContactApi {
	
	@Autowired
	private ContactService contactService;

	@PostMapping
	public ResponseEntity<Contact> save(@RequestBody Contact contact){

		contactService.save( contact);

		return new ResponseEntity<>(contact, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<Contact>> find(@RequestBody Contact contact){

		List<Contact> contactsList = contactService.find( contact);

		return new ResponseEntity<>(contactsList, HttpStatus.OK);
	}

	@DeleteMapping
	public ResponseEntity delete(@RequestBody Contact contact){

		contactService.delete( contact);

		return new ResponseEntity<>(HttpStatus.OK);
	}

}

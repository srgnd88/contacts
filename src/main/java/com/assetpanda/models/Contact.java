package com.assetpanda.models;

public class Contact {
    private String lastName;
    private String firstName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof Contact)) {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        Contact c = (Contact) o;

        // Compare the data members and return accordingly
        return (c.getFirstName() != null
                && c.getFirstName().equalsIgnoreCase( this.getFirstName()))
                && (c.getLastName() != null
                && c.getLastName().equalsIgnoreCase( this.getLastName()));

    }
}
